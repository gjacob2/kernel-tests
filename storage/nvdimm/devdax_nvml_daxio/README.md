# storage/nvdimm/devdax_nvml_daxio

Storage: nvdimm devdax daxio test from nvml

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
