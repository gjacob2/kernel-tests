# storage/nvme_rdma/nvmeof_rdma_host_reboot_during_connect

Storage: nvmeof rdma host reboot during connect, BZ1963290

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
